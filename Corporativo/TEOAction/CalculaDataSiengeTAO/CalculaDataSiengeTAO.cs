﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;

namespace CalculaDataSiengeTAO {
	public class CalculaDataSiengeTAOs : CodeActivity {

		[Output("Data para enviar lembrete")]
		public OutArgument<DateTime> nec_auxdataLembreteSienge { get; set; }

		[Input("Data Kickoff Realizado Interno")]
		public InArgument<DateTime> nec_KickOffInternoRealizado { get; set; }

		protected override void Execute(CodeActivityContext context) {

			DateTime kickoff = nec_KickOffInternoRealizado.Get<DateTime>(context);
			DateTime newData = kickoff.AddHours(72);

			// Se for pro Sabado ou Domingo joga pra segunda
			if (newData.DayOfWeek.Equals(DayOfWeek.Saturday))
				newData = newData.AddDays(2);
			else if (newData.DayOfWeek.Equals(DayOfWeek.Sunday))
				newData = newData.AddDays(1);

			newData = newData.AddHours(DateTime.Now.Hour);
			newData = newData.AddMinutes(DateTime.Now.Minute);
			newData = newData.AddMinutes(5);

			nec_auxdataLembreteSienge.Set(context, newData);

		}

	}
}
