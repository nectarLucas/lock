﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Client; // to get the OrganizationContext
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace AtividadesOcorrencia
{
    public class preencheAtividade : IPlugin {
		
		public void Execute(IServiceProvider serviceProvider) {
			if (serviceProvider == null)
				throw new ArgumentNullException("serviceProvider");

			// Busca contexto e servico
			var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
			var serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
			IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

			// Retorno para evitar loopings infinitos
			if (context.Depth > 1)
				return;

			if (context.PostEntityImages.Contains("task") && context.PostEntityImages["task"] is Entity 
				&& context.PostEntityImages["task"].Attributes.Contains("regardingobjectid") 
				&& context.PostEntityImages["task"].Attributes["regardingobjectid"] is EntityReference){

				if (((EntityReference)context.PostEntityImages["task"].Attributes["regardingobjectid"]).LogicalName == "incident") {
					Guid IncidentId = ((EntityReference)context.PostEntityImages["task"].Attributes["regardingobjectid"]).Id;

					Entity e = service.Retrieve("incident", IncidentId, new ColumnSet(true));

					if (e.Attributes.Contains("nec_aux_atividades")) {
						e.Attributes["nec_aux_atividades"] = e.Attributes["nec_aux_atividades"] + "\n" + context.PostEntityImages["task"].Attributes["description"];
					}else{
						e.Attributes.Add("nec_aux_atividades", context.PostEntityImages["task"].Attributes["description"]);
					}
					service.Update(e);
				}
			}
		}
    }
}
