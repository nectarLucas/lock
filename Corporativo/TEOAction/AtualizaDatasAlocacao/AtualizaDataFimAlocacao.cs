﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Client; // to get the OrganizationContext
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace AtualizaDataFimAlocacao {
	public class AtualizaDataFimAlocacao : IPlugin {

		public void Execute(IServiceProvider serviceProvider) {

			if (serviceProvider == null)
				throw new ArgumentNullException("serviceProvider");

			// Busca contexto e servico
			var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
			var serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
			var service = serviceFactory.CreateOrganizationService(context.UserId);

			// Retorno para evitar loopings infinitos
			if (context.Depth > 1)
				return;

			// verifica se tem as infos necessarias
			if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity
			&& context.PreEntityImages.Contains("oldEngenharia") && context.PreEntityImages["oldEngenharia"] is Entity){

				Entity engenharia = ((Entity)context.InputParameters["Target"]);
				Entity oldEngenharia = (Entity)context.PreEntityImages["oldEngenharia"];
				String dataAntiga;
				string fetchalocacoes;
				string fetchalocacoesXML;

				// Se foi alterado a data fim da obra - linha de base
				if (((Entity)context.InputParameters["Target"]).Contains("nec_datafimdaobralinhadebase")
					&& ((Entity)context.InputParameters["Target"])["nec_datafimdaobralinhadebase"] is DateTime) {


					if (oldEngenharia.Contains("nec_datafimdaobralinhadebase") && oldEngenharia.Attributes["nec_datafimdaobralinhadebase"] is DateTime) {
						dataAntiga = ((DateTime)oldEngenharia.Attributes["nec_datafimdaobralinhadebase"]).ToString("yyyy-MM-dd");
						fetchalocacoes = @"<fetch no-lock='true' >
								<entity name='nec_alocacao' >
									<attribute name='nec_alocacaoid'/>
									<attribute name='nec_data'/>
									<filter>
										<condition attribute='nec_engenhariaid' operator='eq' value='{0}' />
										<condition attribute='nec_data' operator='eq' value='{1}' />
									</filter>
								</entity>
							</fetch>";
						fetchalocacoesXML = String.Format(fetchalocacoes, engenharia.Id, dataAntiga);
					} else {
						dataAntiga = "";
						fetchalocacoes = @"<fetch no-lock='true' >
								<entity name='nec_alocacao' >
									<attribute name='nec_alocacaoid'/>
									<attribute name='nec_data'/>
									<filter>
										<condition attribute='nec_engenhariaid' operator='eq' value='{0}' />
										<condition attribute='nec_data' operator='null' />
									</filter>
								</entity>
							</fetch>";
						fetchalocacoesXML = String.Format(fetchalocacoes, engenharia.Id);
					}
					
					EntityCollection alocacoes = service.RetrieveMultiple(new FetchExpression(fetchalocacoesXML));

					foreach (Entity aloc in alocacoes.Entities) {
						aloc["nec_data"] = engenharia["nec_datafimdaobralinhadebase"];
						service.Update(aloc);
					}
				}

				// Se foi alterado o prazo final da obra repactuado
				if (((Entity)context.InputParameters["Target"]).Contains("nec_prazofinaldaobrarepactuado")
					&& ((Entity)context.InputParameters["Target"])["nec_prazofinaldaobrarepactuado"] is DateTime) {


					if (oldEngenharia.Contains("nec_prazofinaldaobrarepactuado") && oldEngenharia.Attributes["nec_prazofinaldaobrarepactuado"] is DateTime) {
						dataAntiga = ((DateTime)oldEngenharia.Attributes["nec_prazofinaldaobrarepactuado"]).ToString("yyyy-MM-dd");
						fetchalocacoes = @"<fetch no-lock='true' >
								<entity name='nec_alocacao' >
									<attribute name='nec_alocacaoid'/>
									<attribute name='nec_datadefimrepactuada'/>
									<filter>
										<condition attribute='nec_engenhariaid' operator='eq' value='{0}' />
										<condition attribute='nec_datadefimrepactuada' operator='eq' value='{1}' />
									</filter>
								</entity>
							</fetch>";
						fetchalocacoesXML = String.Format(fetchalocacoes, engenharia.Id, dataAntiga);
					} else {
						dataAntiga = "";
						fetchalocacoes = @"<fetch no-lock='true' >
								<entity name='nec_alocacao' >
									<attribute name='nec_alocacaoid'/>
									<attribute name='nec_datadefimrepactuada'/>
									<filter>
										<condition attribute='nec_engenhariaid' operator='eq' value='{0}' />
										<condition attribute='nec_datadefimrepactuada' operator='null' />
									</filter>
								</entity>
							</fetch>";
						fetchalocacoesXML = String.Format(fetchalocacoes, engenharia.Id);
					}

					EntityCollection alocacoes = service.RetrieveMultiple(new FetchExpression(fetchalocacoesXML));

					foreach (Entity aloc in alocacoes.Entities) {
						aloc["nec_datadefimrepactuada"] = engenharia["nec_prazofinaldaobrarepactuado"];
						service.Update(aloc);
					}
				}
			}
		}
	}
}
