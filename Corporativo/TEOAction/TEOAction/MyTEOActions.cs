﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;

namespace TEOAction {

	public class MyTEOActions : CodeActivity {

		#region OUTPUTS

		[Output("Data Prevista Para Realizar CL final com cliente")]
		public OutArgument<DateTime> nec_DataPrevistaParaRealizarCLfinalcomcliente { get; set; }

		[Output ("Data Prevista Para Avaliar os Fornecedores")]
		public OutArgument<DateTime> nec_DataPrevistaParaAvaliarosFornecedores { get; set; }

		[Output ("Data Prevista Para Devolver Equipamentos para TI")]
		public OutArgument<DateTime> nec_DataPrevistaParaDevolverEquipamentosparaT { get; set; }

		[Output ("Data Prevista Envio das Avaliações para Engenharia")]
		public OutArgument<DateTime> nec_DtPrevistaEnviodasAvaliacoesEngenharia { get; set; }

		[Output ("Data Prevista Para Entregar As Built ao MKT")]
		public OutArgument<DateTime> nec_DataPrevistaParaEntregarAsBuiltaoMKT { get; set; }
		
		[Output ("Data Prevista Para Finalizar CL Final com Cliente")]
		public OutArgument<DateTime> nec_DtPrevistaParaFinalizarCLFinalcomCliente { get; set; }
		
		[Output ("Data Prevista Para Pesquisa de Satisfação")]
		public OutArgument<DateTime> nec_DataPrevistaParaPesquisadeSatisfacao { get; set; }

		[Output ("Data Prevista Para Liberação de Todas as Cauções")]
		public OutArgument<DateTime> nec_DataPrevistaParaLiberacaodeTodasasCaucoes { get; set; }

		[Output ("Data Prevista Para Saldos de Pedidos e Contratos")]
		public OutArgument<DateTime> nec_DataPrevistaParaSaldosdePedidoseContratos { get; set; }

		[Output ("Data Prevista Para Encerramento da CEI")]
		public OutArgument<DateTime> nec_DataPrevistaParaEncerramentodaCEI { get; set; }

		[Output ("Data Prevista Para Fechamento Financeiro (PAC)")]
		public OutArgument<DateTime> nec_DataPrevistaParaFechamentoFinanceiroPAC { get; set; }

		[Output ("Data Prevista Para Reunião de Encerramento")]
		public OutArgument<DateTime> nec_DataPrevistaParaReuniaodeEncerramento { get; set; }

		[Output ("Data Prevista Para Realizar feedback com a equipe")]
		public OutArgument<DateTime> nec_DtPrevistaParaRealizarfeedbackcomaequipe { get; set; }

		[Output ("Data Prevista Digitalizar Processo de Encerramento")]
		public OutArgument<DateTime> nec_DtPrevistaDigitalizarProcessoEncerramento { get; set; }

		#endregion

		#region INPUTS
		[Input("Prazo para Realizar CL Final com Cliente")]
		public InArgument<String> nec_PrazoparaRealizarCLfinalcomcliente { get; set; }
		
		[Input("Prazo para Avaliar Fornecedores")]
		public InArgument<String> nec_PrazoparaAvaliarFornecedores { get; set; }

		[Input("Prazo para devolver equipamentos para TI")]
		public InArgument<String> nec_PrazoparaDevolverequipamentosparaTI { get; set; }

		[Input("Prazo para Envio das Avaliacoes para Engenharia")]
		public InArgument<String> nec_PrazoparaEnviodasAvaliacoesparaEngenharia { get; set; }

		[Input("Prazo para Entregar asBuilt ao MKT")]
		public InArgument<String> nec_PrazoparaEntregaAsBuiltaoMKT { get; set; }

		[Input("Prazo para Finalizar CL Final com Cliente")]
		public InArgument<String> nec_PrazoparaFinalizarCLFinalcomCliente { get; set; }

		[Input("Prazo para Pesquisa de Satisfação")]
		public InArgument<String> nec_PrazoparaPesquisadeSatisfacao { get; set; }

		[Input("Prazo para Liberação de Cauções")]
		public InArgument<String> nec_PrazoparaLiberacaodeTodasasCaucoes { get; set; }

		[Input("Prazo para Saldos de Pedidos e Contatos")]
		public InArgument<String> nec_PrazoparaSaldosdePedidoseContratos { get; set; }

		[Input("Prazo para Encerramento de CEI")]
		public InArgument<String> nec_PrazoparaEncerramentodeCEI { get; set; }

		[Input("Prazo para Fechamento Financeiro PAC")]
		public InArgument<String> nec_PrazoparaFechamentoFinanceiroPAC { get; set; }

		[Input("Prazo para Reunião de Encerramento")]
		public InArgument<String> nec_PrazoparaReuniaodeEncerramento { get; set; }

		[Input("Prazo para Realizar Feedback com a Equipe")]
		public InArgument<String> nec_PrazoparaRealizarFeedbackcomaEquipe { get; set; }

		[Input("Prazo para Digitalizar processos de Encerramento")]
		public InArgument<String> nec_PrazoparaDigitalizarProcessodeEncerrament { get; set; }
		
		[Input("Data Final Real")]
		public InArgument<DateTime> nec_DataFinalReal { get; set; }
		#endregion

		protected override void Execute(CodeActivityContext context) {

			nec_DataPrevistaParaRealizarCLfinalcomcliente.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaRealizarCLfinalcomcliente.Get<String>(context))));

			nec_DataPrevistaParaAvaliarosFornecedores.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaAvaliarFornecedores.Get<String>(context))));

			nec_DataPrevistaParaDevolverEquipamentosparaT.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaDevolverequipamentosparaTI.Get<String>(context))));

			nec_DtPrevistaEnviodasAvaliacoesEngenharia.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaEnviodasAvaliacoesparaEngenharia.Get<String>(context))));

			nec_DataPrevistaParaEntregarAsBuiltaoMKT.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaEntregaAsBuiltaoMKT.Get<String>(context))));

			nec_DtPrevistaParaFinalizarCLFinalcomCliente.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaFinalizarCLFinalcomCliente.Get<String>(context))));

			nec_DataPrevistaParaPesquisadeSatisfacao.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaPesquisadeSatisfacao.Get<String>(context))));

			nec_DataPrevistaParaLiberacaodeTodasasCaucoes.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaLiberacaodeTodasasCaucoes.Get<String>(context))));

			nec_DataPrevistaParaSaldosdePedidoseContratos.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaSaldosdePedidoseContratos.Get<String>(context))));

			nec_DataPrevistaParaEncerramentodaCEI.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaEncerramentodeCEI.Get<String>(context))));

			nec_DataPrevistaParaFechamentoFinanceiroPAC.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaFechamentoFinanceiroPAC.Get<String>(context))));

			nec_DataPrevistaParaReuniaodeEncerramento.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaReuniaodeEncerramento.Get<String>(context))));

			nec_DtPrevistaParaRealizarfeedbackcomaequipe.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaRealizarFeedbackcomaEquipe.Get<String>(context))));

			nec_DtPrevistaDigitalizarProcessoEncerramento.Set(context, DiasUteis(nec_DataFinalReal.Get<DateTime>(context), Convert.ToInt32(nec_PrazoparaDigitalizarProcessodeEncerrament.Get<String>(context))));

		}

		public DateTime DiasUteis(DateTime data, int dias) {

			return data.AddDays(dias);
			
			/*
			 * Versão antiga que so contava os dias uteis
			 * 
			DateTime newData = data;

			for (int i = 0; i < dias; i++) {
				newData = newData.AddDays(1);
				// Se for pro Sabado joga pra segunda
				if (newData.DayOfWeek.Equals(DayOfWeek.Saturday))
					newData = newData.AddDays(2);
			}
			
			return newData;

			*/
		}
	}
}
