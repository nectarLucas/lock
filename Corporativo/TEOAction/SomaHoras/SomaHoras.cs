﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;
using Microsoft.Xrm.Sdk.Client; // to get the OrganizationContext
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace SomaHoras
{
    public class SomaHoras : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
                throw new ArgumentNullException("serviceProvider");

            // Busca contexto e servico
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            var service = serviceFactory.CreateOrganizationService(context.UserId);

            // Retorno para evitar loops infinitos
            if (context.Depth > 1)
                return;

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {

                // Pega a ocorrencia 
                Entity targetOcorrencia = service.Retrieve("incident", new Guid(context.PrimaryEntityId.ToString()), new ColumnSet(true));

                // Pega as Horas atuais
                var fetchHoras = @"<fetch no-lock='true' >
						<entity name='nec_horadeatendimento' >
							<attribute name='nec_valortotal'/>
							<filter>
								<condition attribute='nec_ocorrenciaid' operator='eq' value='{0}' />
							</filter>
						</entity>
					</fetch>";
                var fetchHorasXML = String.Format(fetchHoras, targetOcorrencia.Id);
                EntityCollection horas = service.RetrieveMultiple(new FetchExpression(fetchHorasXML));

                // Reinicializa o valor total (necessario, pois o valor total das
                // horas e recalculado a cada atualizacao do registro de ocorrencia.

                decimal total = 0;

                for (int i = 0; i < horas.Entities.Count; i++)
                {
                    //E necessario checar se o registro de Hora contem o campo e se ele e do tipo Money
                    if(horas.Entities[i].Contains("nec_valortotal") && horas.Entities[i]["nec_valortotal"] is Money)
                    total += ((Money)(horas.Entities[i]["nec_valortotal"])).Value;
                }
                
                targetOcorrencia["nec_valortotaldehoras"] = new Money(total);
                service.Update(targetOcorrencia);
            }
        }
    }
}
