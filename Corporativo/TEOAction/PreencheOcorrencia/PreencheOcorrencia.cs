﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;
using Microsoft.Xrm.Sdk.Client; // to get the OrganizationContext
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace PreencheOcorrencia
{
    public class PreencheOcorrencia : IPlugin {
		IOrganizationService service;
		public void Execute(IServiceProvider serviceProvider) {

			if (serviceProvider == null)
				throw new ArgumentNullException("serviceProvider");

			// Busca contexto e servico
			var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
			var serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
			service = serviceFactory.CreateOrganizationService(context.UserId);

			// Retorno para evitar loopings infinitos
			if (context.Depth > 1)
				return;

			if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity) {
				// && ((Entity)context.InputParameters["Target"]).Contains("nec_engenhariaid") 
				// && ((Entity)context.InputParameters["Target"])["nec_engenhariaid"] != null

				// Pega a ocorrencia
				Entity ocorrencia = service.Retrieve("incident", new Guid(context.PrimaryEntityId.ToString()), new ColumnSet(true));
				
				// Caso a ocorrencia possua uma engenharia
				if (ocorrencia.Contains("nec_engenhariaid") && ocorrencia.GetAttributeValue<EntityReference>("nec_engenhariaid") != null) {
					
					// ((Entity)context.InputParameters["Target"]).Contains("nec_engenhariaid")
					// Guid IdEngenharia = ((EntityReference)((Entity)context.InputParameters["Target"])["nec_engenhariaid"]).Id;
					Guid IdEngenharia = ocorrencia.GetAttributeValue<EntityReference>("nec_engenhariaid").Id;

					/* 
					 * ---------- ADICIONA OS COORDENADORES ---------- 
					 */

					// Cria o link Coordenador-Ocorrencia
					Relationship relCoordenador = new Relationship("nec_incident_nec_funcionario_coordenador");

					// Busca os coordenadores atualmente linkados relacionados a ocorrencia
					EntityReferenceCollection RefCoordenador = RetrieveLinksAtuais(relCoordenador.SchemaName, ocorrencia);

					// Remove os coordenadores Atuais
					service.Disassociate("incident", ocorrencia.Id, relCoordenador, RefCoordenador);

					// Busca os gerentes associados (gerentes possuem cargo de codigo 794480000)
					EntityCollection Coordenadores = RetriveFuncionariosAssociados(IdEngenharia, "794480000");

					// Adiciona os novos coordenadores a Ocorrencia
					((Entity)context.InputParameters["Target"]).RelatedEntities.Add(relCoordenador, Coordenadores);


					/* 
					 * ---------- ADICIONA OS GERENTES ---------- 
					 */

					// Cria o link gerente-Ocorrencia
					Relationship relGerente = new Relationship("nec_incident_nec_funcionario_gerente");

					// Busca os coordenadores atualmente linkados relacionados a ocorrencia
					EntityReferenceCollection RefGerente = RetrieveLinksAtuais(relGerente.SchemaName, ocorrencia);

					// Remove os coordenadores Atuais
					service.Disassociate("incident", ocorrencia.Id, relGerente, RefGerente);

					// Busca os gerentes associados (gerentes possuem cargo de codigo 279440000)
					EntityCollection Gerentes = RetriveFuncionariosAssociados(IdEngenharia, "279440000");

					// Adiciona os coordenadores a Ocorrencia
					((Entity)context.InputParameters["Target"]).RelatedEntities.Add(relGerente, Gerentes);


					service.Update(((Entity)context.InputParameters["Target"]));
				}
			}
		}

		public EntityCollection RetriveFuncionariosAssociados(Guid engenharia, String cod_cargo) {
			// Pega as Alocacoes atuais
			var fetchAlocacoes = @"<fetch>
							<entity name='nec_funcionario'>
								<attribute name='nec_funcionarioid'/>
								<attribute name='nec_name'/>
								<link-entity name='nec_alocacao' from='nec_funcionarioid' to='nec_funcionarioid' alias='ab'>
									<filter>
										<condition attribute='nec_engenhariaid' operator='eq' value='{0}'/>
										<condition attribute='nec_cargo' operator='eq' value='{1}'/>
									</filter>
								</link-entity>
							</entity>
						</fetch>"; // statecode == 0 sao os ativos
								   //
			var fetchAlocacoesXML = String.Format(fetchAlocacoes, engenharia, cod_cargo);
			EntityCollection Alocacoes = service.RetrieveMultiple(new FetchExpression(fetchAlocacoesXML));


			return Alocacoes;
		}

		public EntityReferenceCollection RetrieveLinksAtuais(string rel, Entity ocorrencia) {

			var fetchAtuais = @"<fetch>
						<entity name='nec_funcionario'>
							<attribute name='nec_funcionarioid'/>
							<attribute name='nec_name'/>
							<link-entity name='{0}' from='nec_funcionarioid' to='nec_funcionarioid' visible='false' intersect='true'>
								<filter>
									<condition attribute='incidentid' operator='eq' value='{1}'/>
								</filter>
							</link-entity>
						</entity>
					</fetch>";
			var fetchXML = String.Format(fetchAtuais, rel, ocorrencia.Id);
			EntityCollection oldTAOsSecundarias = service.RetrieveMultiple(new FetchExpression(fetchXML));

			// Adiciona as TAOs atuais a uma colecao de referencia

			EntityReferenceCollection RefCollectionSecundaria = new EntityReferenceCollection();
			for (int i = 0; i < oldTAOsSecundarias.Entities.Count; i++) {
				EntityReference r = new EntityReference("i9_tao", oldTAOsSecundarias.Entities[i].Id);
				RefCollectionSecundaria.Add(r);
			}

			return RefCollectionSecundaria;
		}

	}
}
