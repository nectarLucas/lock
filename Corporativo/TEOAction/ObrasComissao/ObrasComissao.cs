﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;
using Microsoft.Xrm.Sdk.Client; // to get the OrganizationContext
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace ObrasComissao {

    public class ObrasComissao : IPlugin {

		public void Execute(IServiceProvider serviceProvider) {
		
			if (serviceProvider == null)
				throw new ArgumentNullException("serviceProvider");

			// Busca contexto e servico
			var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
			var serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
			var service = serviceFactory.CreateOrganizationService(context.UserId);

			// Retorno para evitar loopings infinitos
			if (context.Depth > 1)
				return;
			
			if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity) {

				// Pega a comissao 
				Entity targetComissao = service.Retrieve("nec_comissao", new Guid(context.PrimaryEntityId.ToString()), new ColumnSet(true));
				
				// Caso a comissao possua um proprietario
				if (targetComissao.Contains("ownerid")) {

					// Calculo do periodo desta comissao
					int periodo = Convert.ToInt32(targetComissao["nec_periodo"]);
					int ano = Convert.ToInt32(targetComissao["nec_ano"]);
					var data_inicio = ano + "-" + (3 * (periodo - 1) + 1).ToString() + "-1";
					var data_fim = ano + "-" + (3 * periodo) + "-" + DateTime.DaysInMonth(ano, 3 * periodo);

					// Cria o link TAO-Comissao
					Relationship rel = new Relationship("nec_nec_comissao_i9_tao");

					// Pega as TAOs atuais
					var fetchTAOs = @"<fetch no-lock='true' >
						<entity name='i9_tao' >
							<attribute name='i9_taoid'/>
							<filter>
								<condition attribute='nec_comissaoprincipalid' operator='eq' value='{0}' />
							</filter>
						</entity>
					</fetch>";
					var fetchTAOsXML = String.Format(fetchTAOs, targetComissao.Id);
					EntityCollection oldTAOs = service.RetrieveMultiple(new FetchExpression(fetchTAOsXML));

					// Adiciona as TAOs atuais a uma colecao de referencia
					EntityReferenceCollection RefCollection = new EntityReferenceCollection();
					for (int i = 0; i < oldTAOs.Entities.Count; i++) {
						EntityReference r = new EntityReference("i9_tao", oldTAOs.Entities[i].Id);
						RefCollection.Add(r);
					}

					// Remove as TAOs Atuais da comissao
					service.Disassociate("nec_comissao", targetComissao.Id, rel, RefCollection);
					
					// pega as novas TAOs
					var fetch = @"<fetch no-lock='true' >
							<entity name='i9_tao' >
								<attribute name='i9_taoid'/>
								<filter>
									<condition attribute='ownerid' operator='eq' value='{0}' />
									<condition attribute='createdon' operator='on-or-after' value='{1}' />
									<condition attribute='createdon' operator='on-or-before' value='{2}'/>
								</filter>
							</entity>
						</fetch>";

					var fetchXML = String.Format(fetch, targetComissao.GetAttributeValue<EntityReference>("ownerid").Id, data_inicio, data_fim);
					EntityCollection allTAOs = service.RetrieveMultiple(new FetchExpression(fetchXML));

					// Adiciona a nova TAOs a Comissao
					targetComissao.RelatedEntities.Add(rel, allTAOs);

					/*
					 * Parte para as TAOs referentes a comissao secundaria 
					 */

					// Cria o link TAO-Comissao
					Relationship relSecundaria = new Relationship("nec_nec_comissao_i9_tao_2");
					
					// Pega as TAOs atuais
					var fetchTAOsSecundarias = @"<fetch>
						<entity name='i9_tao'>
							<attribute name='i9_taoid'/>
							<attribute name='i9_name'/>
							<link-entity name='nec_nec_comissao_i9_tao_2' from='i9_taoid' to='i9_taoid' visible='false' intersect='true'>
								<link-entity name='nec_comissao' from='nec_comissaoid' to='nec_comissaoid' alias='ab'>
									<filter>
										<condition attribute='nec_comissaoid' operator='eq' value='{0}'/>
									</filter>
								</link-entity>
							</link-entity>
						</entity>
					</fetch>"; 
					var fetchTAOsSecundariasXML = String.Format(fetchTAOsSecundarias, targetComissao.Id);
					EntityCollection oldTAOsSecundarias = service.RetrieveMultiple(new FetchExpression(fetchTAOsSecundariasXML));

					// Adiciona as TAOs atuais a uma colecao de referencia

					EntityReferenceCollection RefCollectionSecundaria = new EntityReferenceCollection();
					for (int i = 0; i < oldTAOsSecundarias.Entities.Count; i++) {
						EntityReference r = new EntityReference("i9_tao", oldTAOsSecundarias.Entities[i].Id);
						RefCollectionSecundaria.Add(r);
					}

					// Remove as TAOs Atuais
					service.Disassociate("nec_comissao", targetComissao.Id, relSecundaria, RefCollectionSecundaria);

					// Busca todos os usuarios abaixo do usuario desta comissao 
					List<String> listaDeIds = busca(targetComissao.GetAttributeValue<EntityReference>("ownerid").Id.ToString(), service, new List<String>());

					if (listaDeIds.Count() > 0) {
						// monta o fetch para as novas TAOs
						var fetchSecundario_pt1 = @"<fetch mapping='logical' version='1.0'>
							<entity name='i9_tao'>
								<attribute name='i9_taoid' />
								<attribute name='i9_name' />
								<filter>
									<condition attribute = 'createdon' operator='on-or-after' value = '{0}' />
									<condition attribute = 'createdon' operator='on-or-before' value = '{1}' />
									<condition attribute = 'ownerid' operator= 'in'>";
						var fetchSecundario_pt2 = @"
							</condition>
							</filter>
							</entity>
						</fetch>";

						var fetchSecundario = fetchSecundario_pt1;
						foreach (string s in listaDeIds) {
							fetchSecundario = fetchSecundario + "<value>" + s + "</value>";
						}
						fetchSecundario = fetchSecundario + fetchSecundario_pt2;


						#region old Query
						/*
						 "<fetch mapping='logical' version='1.0'>
							<entity name='i9_tao'>
								<attribute name='i9_taoid' />
								<attribute name='i9_name' />
								<filter>
									<condition attribute = 'createdon' operator='on-or-after' value = '{0}' />
									<condition attribute = 'createdon' operator='on-or-before' value = '{1}' />
								</filter>";
						var fetchSecundario_pt2 = @"< link-entity name='systemuser' from='systemuserid' to='ownerid' alias='u' link-type='inner'>
									<attribute name='fullname' />
									<attribute name='parentsystemuserid' />
									<filter>
										<condition attribute='parentsystemuserid' operator='eq' value='{2}' />
									</filter>
								</link-entity>
							</entity>
						</fetch>"
						 */
						#endregion

						// Busca as novas TAOs
						var fetchXMLSecundario = String.Format(fetchSecundario, data_inicio, data_fim);
						EntityCollection allTAOsSecundarias = service.RetrieveMultiple(new FetchExpression(fetchXMLSecundario));

						// Adiciona a nova TAOs a Comissao
						targetComissao.RelatedEntities.Add(relSecundaria, allTAOsSecundarias);
					}

					// Salva o registro
					service.Update(targetComissao);
				}
			}
		}

		public List<String> busca(String idUsuario, IOrganizationService service, List<String> idsUsuarios) {
			//List<String> idsUsuarios = new List<String>();

			var fetchFilhos = @"<fetch no-lock='true' >
						<entity name='systemuser' >
							<attribute name='systemuserid'/>
							<attribute name='fullname'/>
							<filter>
								<condition attribute='parentsystemuserid' operator='eq' value='{0}' />
							</filter>
						</entity>
					</fetch>";
			var fetchFilhosXML = String.Format(fetchFilhos, idUsuario);
			EntityCollection filhos = service.RetrieveMultiple(new FetchExpression(fetchFilhosXML));

			// Versao Recursiva:
			for (int i = 0; i < filhos.Entities.Count; i++) {
				idsUsuarios.Add(filhos.Entities[i].Id.ToString());
				idsUsuarios = busca(filhos.Entities[i].Id.ToString(), service, idsUsuarios);
			}

			#region Versao nao recursiva
			/* 			
			while(true) {
				for (int i = 0; i < filhos.Entities.Count; i++) {
					idsUsuarios.Add(filhos.Entities[i].Id.ToString());
					var novofetchFilhos = @"<fetch no-lock='true' >
						<entity name='systemuser' >
							<attribute name='systemuserid'/>
							<attribute name='fullname'/>
							<filter>
								<condition attribute='parentsystemuserid' operator='eq' value='{0}' />
							</filter>
						</entity>
					</fetch>";
					var novofetchFilhosXML = String.Format(novofetchFilhos, filhos.Entities[i].Id.ToString());
					EntityCollection novosfilhos = service.RetrieveMultiple(new FetchExpression(novofetchFilhosXML));
					filhos.Entities.AddRange(novosfilhos.Entities); 
				}
			}
			*/
			#endregion


			return idsUsuarios;
		}
	}
}
