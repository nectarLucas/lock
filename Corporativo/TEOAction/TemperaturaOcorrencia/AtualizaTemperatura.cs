﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Client; // to get the OrganizationContext
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace TemperaturaOcorrencia {
	public class AtualizaTemperatura : IPlugin {

		public void Execute(IServiceProvider serviceProvider) {

			if (serviceProvider == null)
				throw new ArgumentNullException("serviceProvider");

			// Busca contexto e servico
			var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
			var serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
			IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

			// Retorno para evitar loopings infinitos
			if (context.Depth > 1)
				return;

			if (context.PostEntityImages.Contains("Ocorrencia") && context.PostEntityImages["Ocorrencia"] is Entity
			&& ((Entity)context.PostEntityImages["Ocorrencia"]).Attributes.Contains("nec_temperatura")) {


				Entity ocorrencia = (Entity)context.PostEntityImages["Ocorrencia"];
				// nec_Temperatura
				String fetchOcorrencias = @"<fetch no-lock='true' >
								<entity name='incident' >
									<attribute name='incidentid'/>
									<attribute name='nec_temperatura'/>
									<filter>
										<condition attribute='nec_temperatura' operator='neq' value='{0}' />
										<condition attribute='customerid' operator= 'eq' value='{1}' />
										<condition attribute='statecode' operator= 'eq' value='0' />
									</filter>
								</entity>
							</fetch>";
				String fetchOcorrenciasXML = String.Format(fetchOcorrencias,
					((OptionSetValue)ocorrencia.Attributes["nec_temperatura"]).Value, ((EntityReference)(ocorrencia.Attributes["customerid"])).Id);

				EntityCollection ocorrencias = service.RetrieveMultiple(new FetchExpression(fetchOcorrenciasXML));

				foreach (Entity ocor in ocorrencias.Entities) {
					ocor["nec_temperatura"] = ocorrencia["nec_temperatura"];
					service.Update(ocor);
				}
			}
		}
	}
}
