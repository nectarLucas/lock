﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Microsoft.Xrm.Sdk.Workflow;
//using System.Activities;
//using Microsoft.Xrm.Sdk.Client; // to get the OrganizationContext
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace TravaFluxoPagamento
{
	public class TravaFluxo : IPlugin {
		public void Execute(IServiceProvider serviceProvider) {

			if (serviceProvider == null)
				throw new ArgumentNullException("serviceProvider");

			// Busca contexto e servico
			var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
			var serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
			var service = serviceFactory.CreateOrganizationService(context.UserId);

			// Retorno para evitar loopings infinitos
			if (context.Depth > 1)
				return;

			if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity) {

				// Pega o CNPJ atual
				Entity oldtargetCNPJ = service.Retrieve(context.PrimaryEntityName, new Guid(context.PrimaryEntityId.ToString()), new ColumnSet(true));
				Entity targetCNPJ = (Entity)context.InputParameters["Target"];

				//							Se possui  b aberto,  se o b aberto  eh  dinheiro, se eh  diferente de 0
				//							Se possui  b diluido, se o b diluido eh  dinheiro, se eh  diferente de 0
				// Se nao possui b aberto,  Se possuia b aberto,  se o b aberto  era dinheiro, se era diferente de 0
				// Se nao possui b diluido, Se possuia b diluido, se o b diluido era dinheiro, se era diferente de 0
				// tl,dr: Se tiver mudado b aberto ou b diluido pra algo diferente de 0 ou se nao tiver mudado mas o valor original for diferente de 0
				if ((targetCNPJ.Contains("i9_b_arquitetura_aberto") && targetCNPJ["i9_b_arquitetura_aberto"] is Money && ((Money)targetCNPJ["i9_b_arquitetura_aberto"]).Value != 0) || 
					(targetCNPJ.Contains("i9_b_arquitetura_diluido") && targetCNPJ["i9_b_arquitetura_diluido"] is Money && ((Money)targetCNPJ["i9_b_arquitetura_diluido"]).Value != 0) ||
					(!targetCNPJ.Contains("i9_b_arquitetura_aberto") && oldtargetCNPJ.Contains("i9_b_arquitetura_aberto") && oldtargetCNPJ["i9_b_arquitetura_aberto"] is Money && ((Money)oldtargetCNPJ["i9_b_arquitetura_aberto"]).Value != 0) ||
					(!targetCNPJ.Contains("i9_b_arquitetura_diluido") && oldtargetCNPJ.Contains("i9_b_arquitetura_diluido") && oldtargetCNPJ["i9_b_arquitetura_diluido"] is Money && ((Money)oldtargetCNPJ["i9_b_arquitetura_diluido"]).Value != 0)) {



						// Pega os fluxos de pagamentos atuais
						var fetchFluxos = @"<fetch no-lock='true' >
							<entity name='nec_fluxodepagamentoparaarquitetura' >
								<attribute name='nec_fluxodepagamentoparaarquiteturaid'/>
								<attribute name='nec_name'/>
								<attribute name='nec_cnpjid'/>
								<filter>
									<condition attribute='nec_cnpjid' operator='eq' value='{0}' />
								</filter>
							</entity>
						</fetch>";

					var fetchFluxosXML = String.Format(fetchFluxos, targetCNPJ.Id);
					EntityCollection Fluxos = service.RetrieveMultiple(new FetchExpression(fetchFluxosXML));

					if (Fluxos.Entities.Count < 1)
						throw new InvalidPluginExecutionException("Fluxo de Pagamento Obrigatório, preencha o fluxo abaixo relacionado ao pagamento da arquitetura!");
					
				}
			}
		}
	}
}