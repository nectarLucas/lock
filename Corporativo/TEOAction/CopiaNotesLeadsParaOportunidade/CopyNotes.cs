﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;


/*
 * https://community.dynamics.com/crm/f/microsoft-dynamics-crm-forum/271311/how-to-pass-notes-from-lead-to-opportunity-after-qualifying-the-lead
 * 
 * 
 */
namespace CopiaNotesLeadsParaOportunidade {

	public class CopyNotes : IPlugin {

		public void Execute(IServiceProvider serviceprovider) {
			// Extract the tracing service for use in debugging sandboxed plug-ins.
			ITracingService tracing = (ITracingService)serviceprovider.GetService(typeof(ITracingService));
			IPluginExecutionContext context = (IPluginExecutionContext)serviceprovider.GetService(typeof(IPluginExecutionContext));
			IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceprovider.GetService(typeof(IOrganizationServiceFactory));
			IOrganizationService service = factory.CreateOrganizationService(context.UserId);

			//Check context contain entity
			if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity) {

				//Check weather the Target entity have the originating lead filed or not
				Entity selectedEntity = (Entity)context.InputParameters["Target"];

				if (selectedEntity.Attributes.Contains("originatingleadid") == false) {
					return;
				} else {

					EntityReference entLead = (EntityReference)selectedEntity.Attributes["originatingleadid"];
					Guid LeadGuid = entLead.Id;

					try {

						if (context.MessageName == "Create") {
							CopiaPosts(LeadGuid, tracing, context, service);
							CopiaInteligencia(LeadGuid, tracing, context, service);
						}
					} catch (Exception ex) {
						tracing.Trace("Exception : " + ex.Message);
						throw new InvalidPluginExecutionException(ex.Message.ToString());
					}
				}
			}
		}

		private void CopiaPosts(Guid LeadGuid, ITracingService tracing, IPluginExecutionContext context, IOrganizationService service) {

			//Busca as postagens do lead
			string strFetchNotes = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
				<entity name='post'>
					<filter type='and'>
						<condition attribute='source' operator='eq' value='2' />
					</filter>
					<link-entity name='lead' from='leadid' to='regardingobjectid' alias='aa'>
						<filter type='and'>
							<condition attribute='leadid' operator='eq' value='{0}' />
						</filter>
					</link-entity>
				</entity>
			</fetch>";

			strFetchNotes = string.Format(strFetchNotes, LeadGuid);

			EntityCollection entNotes = (EntityCollection)service.RetrieveMultiple(new FetchExpression(strFetchNotes));

			if (entNotes != null && entNotes.Entities.Count > 0) {

				for (int i = 0; i < entNotes.Entities.Count; i++) {
									
					Entity entNote = (Entity)entNotes.Entities[i];

					string MODIFICADOPOR = ((EntityReference)entNote.Attributes["modifiedby"]).Name;

					TimeZoneInfo localZone = TimeZoneInfo.Local;
					TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(localZone.Id);
					DateTime ModifiedOn = Convert.ToDateTime(entNote.Attributes["modifiedon"].ToString());
					DateTime userConvertedTime = TimeZoneInfo.ConvertTimeFromUtc(ModifiedOn, tz);

					string DATAMODIFICACAO = userConvertedTime.ToString("dd/MM/yy H:mm:ss tt") + " " + ("(" + localZone.DisplayName + ")");

					Entity entNewPost = new Entity("post");
									
					entNewPost.Attributes.Add("regardingobjectid", new EntityReference(context.PrimaryEntityName, context.PrimaryEntityId));
					string TEXTO = "Postagem feita no Cliente Potencial por: " + MODIFICADOPOR +  "\r\n\r\n" + entNote.GetAttributeValue<string>("text") + " Data do Post: " + DATAMODIFICACAO;
					entNewPost.Attributes.Add("text", TEXTO);
									
					service.Create(entNewPost);
									
				}
			}	
		}

		public void CopiaInteligencia(Guid LeadGuid, ITracingService tracing, IPluginExecutionContext context, IOrganizationService service) {
			// Busca as inteligencias do lead

			string strFetchInteligencia = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
				<entity name='nec_inteligenciademercado_potencial'>
					<link-entity name='lead' from='leadid' to='nec_clientepotencialid' alias='aa'>
						<filter type='and'>
							<condition attribute='leadid' operator='eq' value='{0}' />
						</filter>
					</link-entity>
				</entity>
			</fetch>";

			strFetchInteligencia = string.Format(strFetchInteligencia, LeadGuid);

			EntityCollection entInteligencias = (EntityCollection)service.RetrieveMultiple(new FetchExpression(strFetchInteligencia));

			if (entInteligencias != null && entInteligencias.Entities.Count > 0) {

				for (int i = 0; i < entInteligencias.Entities.Count; i++) {

					Entity entInt = (Entity)entInteligencias.Entities[i];

					Entity entNewInt = new Entity("nec_inteligenciademercado_oportunidade");

					entNewInt.Attributes.Add("nec_name", entInt.GetAttributeValue<String>("nec_name"));
					entNewInt.Attributes.Add("nec_oportunidadeid", new EntityReference(context.PrimaryEntityName, context.PrimaryEntityId));
					entNewInt.Attributes.Add("nec_concorrenteid", entInt.GetAttributeValue<EntityReference>("nec_concorrenteid"));
					entNewInt.Attributes.Add("nec_parceiroid", entInt.GetAttributeValue<EntityReference>("nec_parceiroid"));
					

					service.Create(entNewInt);

				}
			}
		}


		/// <summary>
		/// Creates Cloned copy of soure entity for the given entity object
		/// </summary>
		/// <param name="targetEntityName">Schemaname of the cloned copy target entity</param>
		/// <param name="sourceEntity">Entity object of the record to be Cloned</param>
		/// <param name="strAttributestoRemove">Array of records not to include in the cloned copy</param>
		/// <returns>Entity object (Cloned Copy)</returns>

		private Entity CloneRecordForEntity(string targetEntityName, Entity sourceEntity, string[] strAttributestoRemove) {

			//Initiate target entity (cloned copy)
			Entity clonedEntity = new Entity(targetEntityName);

			//read the attributes from source entity
			AttributeCollection attributeKeys = sourceEntity.Attributes;

			//Loop through each of the key and check and add that in the target entity
			foreach (string key in attributeKeys.Keys) {

				//Check if key is not there in the list of removed keys
				if (Array.IndexOf(strAttributestoRemove, key) == -1) {

					//add key from source in the destination
					clonedEntity[key] = sourceEntity[key];
				} //end if checking removed attributes

			}
			
			//end foreach keys
			return clonedEntity;
		}
	}
}