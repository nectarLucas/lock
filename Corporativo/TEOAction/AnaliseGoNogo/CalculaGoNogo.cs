﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;

namespace AnaliseGoNogo {
    public class CalculaGoNogo : CodeActivity {

		[Output("Resultado Avaliação")]
		public OutArgument<String> lsk_resultadoavaliacao { get; set; }

		[Output("Resultado Avaliação Outdoor")]
		public OutArgument<String> nec_resultadoavaliacaooutdoor { get; set; }

		#region AVALIACOES 

		[Input("Avaliacao Faturamento")]
		public InArgument<String> lsk_avaliacaofaturamento { get; set; }

		[Input("Avaliacao Metragem")]
		public InArgument<String> lsk_avaliacaometragem { get; set; }

		[Input("Avaliacao Relacionamento Cliente")]
		public InArgument<String> lsk_avaliacaorelacionamentocliente { get; set; }

		[Input("Avaliacao Relacionamento Arquiteto")]
		public InArgument<String> lsk_avaliaorelacionamentoarquiteto { get; set; }

		[Input("Avaliacao Importancia")]
		public InArgument<String> lsk_avaliacaoimportancia { get; set; }

		[Input("Avaliacao Disponibilidade")]
		public InArgument<String> lsk_avaliacaodisponibilidade { get; set; }

		[Input("Avaliacao Concorrencia")]
		public InArgument<String> lsk_avaliacaoconcorrencia { get; set; }

		[Input("Avaliacao Gerenciador")]
		public InArgument<String> lsk_avaliacaogerenciador { get; set; }

		[Input("Avaliacao Condominio")]
		public InArgument<String> lsk_avaliacaocondominio { get; set; }

		[Input("Avaliacao Distancia")]
		public InArgument<String> lsk_avaliacaodistancia { get; set; }

		[Input("Avaliacao Margem Prevista")]
		public InArgument<String> nec_avaliacaomargemprevista { get; set; }

		[Input("Avaliacao Valor Contrato")]
		public InArgument<String> nec_avaliacaovalortotaldocontrato { get; set; }

		[Input("Avaliacao Tipo da Obra")]
		public InArgument<String> nec_avaliacaotipodaobra { get; set; }

		[Input("Avaliacao Prazo da Obra")]
		public InArgument<String> nec_avaliacaoprazodaobra { get; set; }

		[Input("Avaliacao Certificacao/Normas de Desempenho")]
		public InArgument<String> nec_AvaliacaoCertificacaoNormasdedesempenho { get; set; }

		[Input("Avaliacao Taxa de Administracao")]
		public InArgument<String> nec_AvaliacaoTaxadeAdministracao { get; set; }

		[Input("Avaliacao Modelo de Contrato")]
		public InArgument<String> nec_AvaliacaoModelodeContrato { get; set; }

		[Input("Avaliacao Expectativa Beneficio")]
		public InArgument<String> nec_AvaliacaoExpectativadeBeneficioLock { get; set; }

		[Input("Avaliacao Setor da Obra Contrato")]
		public InArgument<String> nec_AvaliacaoSetordaObra { get; set; }

		[Input("Avaliacao Prazo Negociavel")]
		public InArgument<String> nec_AvaliacaoPrazoNegociavel { get; set; }



		#endregion

		#region PESO CORPORATIVO
		[Input("Peso Faturamento")]
		public InArgument<String> nec_PesoFeeMes { get; set; }

		[Input("Peso Metragem")]
		public InArgument<String> nec_PesoMetragem { get; set; }

		[Input("Peso Relacionamento Cliente")]
		public InArgument<String> nec_PesoRelacionamentoCliente { get; set; }

		[Input("Peso Relacionamento Arquiteto")]
		public InArgument<String> nec_PesoRelacionamentoArquiteto { get; set; }

		[Input("Peso Importancia Estrategica")]
		public InArgument<String> nec_PesoImportanciaEstrategica { get; set; }

		[Input("Peso Disponibilidade")]
		public InArgument<String> nec_PesoDisponibilidade { get; set; }

		[Input("Peso Concorrencia")]
		public InArgument<String> nec_PesoConcorrencia { get; set; }
			
		[Input("Peso Gerenciador")]
		public InArgument<String> nec_PesoGerenciador { get; set; }

		[Input("Peso Conhecimento")]
		public InArgument<String> nec_PesoConhecimento { get; set; }

		[Input("Peso Distancia")]
		public InArgument<String> nec_PesoDistancia { get; set; }

		[Input("Peso Margem")]
		public InArgument<String> nec_PesoMargem { get; set; }

		[Input("Peso Valor Contrato")]
		public InArgument<String> nec_PesoValorContrato { get; set; }
		#endregion

		#region PESO OUTDOOR

		[Input("Peso Relacionamento Cliente Outdoor")]
		public InArgument<String> nec_PesoRelacionamentoClienteOutdoor { get; set; }

		[Input("Peso Importancia Estrategica Outdoor")]
		public InArgument<String> nec_PesoImportanciaEstrategicaOutdoor { get; set; }

		[Input("Peso Distancia Outdoor")]
		public InArgument<String> nec_PesoDistanciadaObraOutdoor { get; set; }

		[Input("Peso Tipo da Obra Outdoor")]
		public InArgument<String> nec_PesoTipodaObraOutdoor { get; set; }

		[Input("Peso Metragem da Obra Outdoor")]
		public InArgument<String> nec_PesoMetragemdaObraOutdoor { get; set; }

		[Input("Peso Prazo da Obra Outdoor")]
		public InArgument<String> nec_PesoPrazodaObraOutdoor { get; set; }

		[Input("Peso Estimativa Area de Negocios Outdoor")]
		public InArgument<String> nec_PesoEstimativaareadenegociosOutdoor { get; set; }

		[Input("Peso Taxa de Administração Outdoor")]
		public InArgument<String> nec_PesoTaxadeAdministracaoOutdoor { get; set; }

		[Input("Peso Modelo de Contrato Outdoor")]
		public InArgument<String> nec_PesoModelodeContratoOutdoor { get; set; }

		[Input("Peso Expectativa Beneficio Lock Outdoor")]
		public InArgument<String> nec_PesoExpectativadeBeneficioLockOutdoor { get; set; }

		[Input("Peso Concorrencia Outdoor")]
		public InArgument<String> nec_PesoConcorrenciaOutdoor { get; set; }

		[Input("Peso Setor da Obra Outdoor")]
		public InArgument<String> nec_PesoSetordaObraOutdoor { get; set; }

		[Input("Peso Prazo Negociavel Outdoor")]
		public InArgument<String> nec_PesoPrazoNegociavelOutdoor { get; set; }

		[Input("Peso Certificação Outdoor")]
		public InArgument<String> nec_pesocertificacaonormasdedesempenhooutdoor { get; set; }

		#endregion

		protected override void Execute(CodeActivityContext context) {

			Double nota = 0D; 
			Double notaOutdoor = 0D; 

			// Le cada um dos atributos e tenta converte-los para inteiro, se nao conseguir define como 0
			int faturamento = (Int32.TryParse(lsk_avaliacaofaturamento.Get<String>(context), out faturamento) ? faturamento : 0);
			int metragem = (Int32.TryParse(lsk_avaliacaometragem.Get<String>(context), out metragem) ? metragem : 0);
			int relCliente = (Int32.TryParse(lsk_avaliacaorelacionamentocliente.Get<String>(context), out relCliente) ? relCliente : 0);
			int relArquiteto = (Int32.TryParse(lsk_avaliaorelacionamentoarquiteto.Get<String>(context), out relArquiteto) ? relArquiteto : 0);
			int importancia = (Int32.TryParse(lsk_avaliacaoimportancia.Get<String>(context), out importancia) ? importancia : 0);
			int disponibilidade = (Int32.TryParse(lsk_avaliacaodisponibilidade.Get<String>(context), out disponibilidade) ? disponibilidade : 0);
			int concorrencia = (Int32.TryParse(lsk_avaliacaoconcorrencia.Get<String>(context), out concorrencia) ? concorrencia : 0);
			int gerenciador = (Int32.TryParse(lsk_avaliacaogerenciador.Get<String>(context), out gerenciador) ? gerenciador : 0);
			int condominio = (Int32.TryParse(lsk_avaliacaocondominio.Get<String>(context), out condominio) ? condominio : 0);
			int distancia = (Int32.TryParse(lsk_avaliacaodistancia.Get<String>(context), out distancia) ? distancia : 0);
			int margem = (Int32.TryParse(nec_avaliacaomargemprevista.Get<String>(context), out margem) ? margem : 0);
			int valorContrato = (Int32.TryParse(nec_avaliacaovalortotaldocontrato.Get<String>(context), out valorContrato) ? valorContrato : 0);
			int tipodaObra = (Int32.TryParse(nec_avaliacaotipodaobra.Get<String>(context), out tipodaObra) ? tipodaObra : 0);
			int prazoObra = (Int32.TryParse(nec_avaliacaoprazodaobra.Get<String>(context), out prazoObra) ? prazoObra : 0);
			int certificacaoNormasDesempenho = (Int32.TryParse(nec_AvaliacaoCertificacaoNormasdedesempenho.Get<String>(context), out certificacaoNormasDesempenho) ? certificacaoNormasDesempenho : 0);
			int taxaAdministracao = (Int32.TryParse(nec_AvaliacaoTaxadeAdministracao.Get<String>(context), out taxaAdministracao) ? taxaAdministracao : 0);
			int modeloContrato = (Int32.TryParse(nec_AvaliacaoModelodeContrato.Get<String>(context), out modeloContrato) ? modeloContrato : 0);
			int expectativaBeneficio = (Int32.TryParse(nec_AvaliacaoExpectativadeBeneficioLock.Get<String>(context), out expectativaBeneficio) ? expectativaBeneficio : 0);
			int setorObra = (Int32.TryParse(nec_AvaliacaoSetordaObra.Get<String>(context), out setorObra) ? setorObra : 0);
			int prazoNegociavel = (Int32.TryParse(nec_AvaliacaoPrazoNegociavel.Get<String>(context), out prazoNegociavel) ? prazoNegociavel : 0);
			


			// Le cada um dos pesos e tenta converte-los para inteiro, se nao conseguir define como um valor padrão (inicial)
			int pesoFaturamento = (Int32.TryParse(nec_PesoFeeMes.Get<String>(context), out pesoFaturamento) ? pesoFaturamento : 18);
			int pesoMetragem = (Int32.TryParse(nec_PesoMetragem.Get<String>(context), out pesoMetragem) ? pesoMetragem : 10);
			int pesoRelCliente = (Int32.TryParse(nec_PesoRelacionamentoCliente.Get<String>(context), out pesoRelCliente) ? pesoRelCliente : 12);
			int pesoRelArquiteto = (Int32.TryParse(nec_PesoRelacionamentoArquiteto.Get<String>(context), out pesoRelArquiteto) ? pesoRelArquiteto : 10);
			int pesoImportancia = (Int32.TryParse(nec_PesoImportanciaEstrategica.Get<String>(context), out pesoImportancia) ? pesoImportancia : 10);
			int pesoDisponibilidade = (Int32.TryParse(nec_PesoDisponibilidade.Get<String>(context), out pesoDisponibilidade) ? pesoDisponibilidade : 2);
			int pesoConcorrencia = (Int32.TryParse(nec_PesoConcorrencia.Get<String>(context), out pesoConcorrencia) ? pesoConcorrencia : 10);
			int pesoGerenciador = (Int32.TryParse(nec_PesoGerenciador.Get<String>(context), out pesoGerenciador) ? pesoGerenciador : 5);
			int pesoCondominio = (Int32.TryParse(nec_PesoConhecimento.Get<String>(context), out pesoCondominio) ? pesoCondominio : 5);
			int pesoDistancia = (Int32.TryParse(nec_PesoDistancia.Get<String>(context), out pesoDistancia) ? pesoDistancia : 2);
			int pesoMargem = (Int32.TryParse(nec_PesoMargem.Get<String>(context), out pesoMargem) ? pesoMargem : 10);
			int pesoValorContrato = (Int32.TryParse(nec_PesoValorContrato.Get<String>(context), out pesoValorContrato) ? pesoValorContrato : 6);
			int pesoRelClienteOutdoor = (Int32.TryParse(nec_PesoRelacionamentoClienteOutdoor.Get<String>(context), out pesoRelClienteOutdoor)? pesoRelClienteOutdoor: 15);
			int pesoImportanciaOutdoor = (Int32.TryParse(nec_PesoImportanciaEstrategicaOutdoor.Get<String>(context), out pesoImportanciaOutdoor)? pesoImportanciaOutdoor: 5);
			int pesoDistanciaOutdoor = (Int32.TryParse(nec_PesoDistanciadaObraOutdoor.Get<String>(context), out pesoDistanciaOutdoor)? pesoDistanciaOutdoor: 3);
			int pesoTipoObraOutdoor = (Int32.TryParse(nec_PesoTipodaObraOutdoor.Get<String>(context), out pesoTipoObraOutdoor)? pesoTipoObraOutdoor: 4);
			int pesoMetragemOutdoor = (Int32.TryParse(nec_PesoMetragemdaObraOutdoor.Get<String>(context), out pesoMetragemOutdoor)? pesoMetragemOutdoor: 15);
			int pesoPrazoOutdoor = (Int32.TryParse(nec_PesoPrazodaObraOutdoor.Get<String>(context), out pesoPrazoOutdoor)? pesoPrazoOutdoor: 5);
			int pesoValorContratoOutdoor = (Int32.TryParse(nec_PesoEstimativaareadenegociosOutdoor.Get<String>(context), out pesoValorContratoOutdoor) ? pesoValorContratoOutdoor : 3);
			int pesoTaxaAdmOutdoor = (Int32.TryParse(nec_PesoTaxadeAdministracaoOutdoor.Get<String>(context), out pesoTaxaAdmOutdoor)? pesoTaxaAdmOutdoor: 10);
			int pesoModeloContratoOutdoor = (Int32.TryParse(nec_PesoModelodeContratoOutdoor.Get<String>(context), out pesoModeloContratoOutdoor)? pesoModeloContratoOutdoor: 10);
			int pesoExpectativaBeneficioOutdoor = (Int32.TryParse(nec_PesoExpectativadeBeneficioLockOutdoor.Get<String>(context), out pesoExpectativaBeneficioOutdoor)? pesoExpectativaBeneficioOutdoor: 15);
			int pesoConcorrenciaOutdoor = (Int32.TryParse(nec_PesoConcorrenciaOutdoor.Get<String>(context), out pesoConcorrenciaOutdoor)? pesoConcorrenciaOutdoor: 3);
			int pesoSetorOutdoor = (Int32.TryParse(nec_PesoSetordaObraOutdoor.Get<String>(context), out pesoSetorOutdoor)? pesoSetorOutdoor: 2);
			int pesoPrazoNegociavelOutdoor = (Int32.TryParse(nec_PesoPrazoNegociavelOutdoor.Get<String>(context), out pesoPrazoNegociavelOutdoor)? pesoPrazoNegociavelOutdoor: 5);
			int pesoCertificacaoOutdoor = (Int32.TryParse(nec_pesocertificacaonormasdedesempenhooutdoor.Get<String>(context), out pesoCertificacaoOutdoor) ? pesoCertificacaoOutdoor : 5);


			nota = ((faturamento*pesoFaturamento) + (metragem * pesoMetragem) + (relCliente * pesoRelCliente) + (relArquiteto * pesoRelArquiteto) 
				+ (importancia * pesoImportancia) + (disponibilidade * pesoDisponibilidade) + (concorrencia * pesoConcorrencia) + (gerenciador * pesoGerenciador) 
				+ (condominio * pesoCondominio) + (distancia * pesoDistancia) + (margem * pesoMargem) + (valorContrato * pesoValorContrato))/100d;

			notaOutdoor = ((relCliente * pesoRelClienteOutdoor) + (importancia * pesoImportanciaOutdoor) + (distancia * pesoDistanciaOutdoor) 
				+ (tipodaObra * pesoTipoObraOutdoor) + (metragem * pesoMetragemOutdoor) + (prazoObra * pesoPrazoOutdoor) + (valorContrato * pesoValorContratoOutdoor) 
				+ (taxaAdministracao * pesoTaxaAdmOutdoor) + (modeloContrato * pesoModeloContratoOutdoor) + (expectativaBeneficio * pesoExpectativaBeneficioOutdoor) 
				+ (concorrencia * pesoConcorrenciaOutdoor) + (setorObra * pesoSetorOutdoor) + (prazoNegociavel * pesoPrazoNegociavelOutdoor)
				+ (certificacaoNormasDesempenho * pesoCertificacaoOutdoor)) /100d;

			lsk_resultadoavaliacao.Set(context, nota.ToString());
			nec_resultadoavaliacaooutdoor.Set(context, notaOutdoor.ToString());
		}
	}
}
