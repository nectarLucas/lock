
	var httpWebRequest = (HttpWebRequest)WebRequest.Create(url + "assembleia/ConsultarAssembleiaPeloID/" + idintegracao + "/" + empresa);
	httpWebRequest.ContentType = "application/json";
	httpWebRequest.Method = "GET";
	httpWebRequest.Headers["Authorization"] = token;



	var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();



	using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
	{
		var result = streamReader.ReadToEnd();
		return result;
	}