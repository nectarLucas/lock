function OnLoad(executionContext) {
	
	
	Xrm.WebApi.retrieveRecord("nec_teo", contactId, "?$select=fullname").
        then(
            function success(teo) {
                var formContext = executionContext.getFormContext(); // get formContext

				if(formContext.getAttribute("TODO").getValue() == ""){
					v = adicionaDiasUteis(formContext.getAttribute("nec_datafinalreal").getValue(), formContext.getAttribute("diasadicionais").getValue());
					formContext.getAttribute("TODO").setValue(v);
				}
            },
            function (error) {
                alert(error.message);
            }
        );
    
}

function adicionaDiasUteis(data, dias){
	data.setDate(data.getDate() + dias);
	
	if(data.getDay() == 6){
		data.setDate(data.getDate() + 2);
	}else if(data.getDay() == 0){
		data.setDate(data.getDate() +1);
	}
	
	return data;
}
