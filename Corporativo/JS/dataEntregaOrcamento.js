function alteraData(campo1, campo2){
	if(Xrm.Page.getAttribute(campo1).getValue() == true){
		Xrm.Page.getAttribute(campo2).setValue(new Date());
	}else{
		Xrm.Page.getAttribute(campo2).setValue("");
	}	
}

function bloqueiaCampos(){
	if(Xrm.Page.getControl("i9_dataconclusao01")) Xrm.Page.getControl("i9_dataconclusao01").setDisabled(true);
	if(Xrm.Page.getControl("i9_dataconcluso02")) Xrm.Page.getControl("i9_dataconcluso02").setDisabled(true);
	if(Xrm.Page.getControl("i9_dataconcluso03")) Xrm.Page.getControl("i9_dataconcluso03").setDisabled(true);
	if(Xrm.Page.getControl("i9_dataconcluso04")) Xrm.Page.getControl("i9_dataconcluso04").setDisabled(true);
	if(Xrm.Page.getControl("i9_dataconcluso05")) Xrm.Page.getControl("i9_dataconcluso05").setDisabled(true);
	if(Xrm.Page.getControl("i9_dataconcluso06")) Xrm.Page.getControl("i9_dataconcluso06").setDisabled(true);
	if(Xrm.Page.getControl("i9_dataconcluso07")) Xrm.Page.getControl("i9_dataconcluso07").setDisabled(true);
	if(Xrm.Page.getControl("i9_dataconcluso08")) Xrm.Page.getControl("i9_dataconcluso08").setDisabled(true);

	if(Xrm.Page.getControl("i9_orcamento_dtentregamodelagem")) Xrm.Page.getControl("i9_orcamento_dtentregamodelagem").setDisabled(true);
	if(Xrm.Page.getControl("i9_datadeconcluso11")) Xrm.Page.getControl("i9_datadeconcluso11").setDisabled(true);
	if(Xrm.Page.getControl("i9_datadeconcluso12")) Xrm.Page.getControl("i9_datadeconcluso12").setDisabled(true);
	if(Xrm.Page.getControl("i9_datadeconcluso13")) Xrm.Page.getControl("i9_datadeconcluso13").setDisabled(true);
	if(Xrm.Page.getControl("i9_datadeconcluso14")) Xrm.Page.getControl("i9_datadeconcluso14").setDisabled(true);
	if(Xrm.Page.getControl("i9_datadeconcluso15")) Xrm.Page.getControl("i9_datadeconcluso15").setDisabled(true);
	if(Xrm.Page.getControl("i9_datadeconcluso16")) Xrm.Page.getControl("i9_datadeconcluso16").setDisabled(true);
	if(Xrm.Page.getControl("i9_dataconcluso17")) Xrm.Page.getControl("i9_dataconcluso17").setDisabled(true);
	if(Xrm.Page.getControl("i9_datadeconcluso18")) Xrm.Page.getControl("i9_datadeconcluso18").setDisabled(true);
	if(Xrm.Page.getControl("i9_datadeconcluso19")) Xrm.Page.getControl("i9_datadeconcluso19").setDisabled(true);
}

function desbloqueiaCampo(){
	if(Xrm.Page.getControl("i9_dataconclusao01")) Xrm.Page.getControl("i9_dataconclusao01").setDisabled(false);
	if(Xrm.Page.getControl("i9_dataconcluso02")) Xrm.Page.getControl("i9_dataconcluso02").setDisabled(false);
	if(Xrm.Page.getControl("i9_dataconcluso03")) Xrm.Page.getControl("i9_dataconcluso03").setDisabled(false);
	if(Xrm.Page.getControl("i9_dataconcluso04")) Xrm.Page.getControl("i9_dataconcluso04").setDisabled(false);
	if(Xrm.Page.getControl("i9_dataconcluso05")) Xrm.Page.getControl("i9_dataconcluso05").setDisabled(false);
	if(Xrm.Page.getControl("i9_dataconcluso06")) Xrm.Page.getControl("i9_dataconcluso06").setDisabled(false);
	if(Xrm.Page.getControl("i9_dataconcluso07")) Xrm.Page.getControl("i9_dataconcluso07").setDisabled(false);
	if(Xrm.Page.getControl("i9_dataconcluso08")) Xrm.Page.getControl("i9_dataconcluso08").setDisabled(false);

	if(Xrm.Page.getControl("i9_orcamento_dtentregamodelagem")) Xrm.Page.getControl("i9_orcamento_dtentregamodelagem").setDisabled(false);
	if(Xrm.Page.getControl("i9_datadeconcluso11")) Xrm.Page.getControl("i9_datadeconcluso11").setDisabled(false);
	if(Xrm.Page.getControl("i9_datadeconcluso12")) Xrm.Page.getControl("i9_datadeconcluso12").setDisabled(false);
	if(Xrm.Page.getControl("i9_datadeconcluso13")) Xrm.Page.getControl("i9_datadeconcluso13").setDisabled(false);
	if(Xrm.Page.getControl("i9_datadeconcluso14")) Xrm.Page.getControl("i9_datadeconcluso14").setDisabled(false);
	if(Xrm.Page.getControl("i9_datadeconcluso15")) Xrm.Page.getControl("i9_datadeconcluso15").setDisabled(false);
	if(Xrm.Page.getControl("i9_datadeconcluso16")) Xrm.Page.getControl("i9_datadeconcluso16").setDisabled(false);
	if(Xrm.Page.getControl("i9_dataconcluso17")) Xrm.Page.getControl("i9_dataconcluso17").setDisabled(false);
	if(Xrm.Page.getControl("i9_datadeconcluso18")) Xrm.Page.getControl("i9_datadeconcluso18").setDisabled(false);
	if(Xrm.Page.getControl("i9_datadeconcluso19")) Xrm.Page.getControl("i9_datadeconcluso19").setDisabled(false);

}

function dataAbertura(){

	var startdate = Xrm.Page.getAttribute("i9_data_abertura_orcamento")? Xrm.Page.getAttribute("i9_data_abertura_orcamento").getValue() : null;
	var tipo_orcamento = Xrm.Page.getAttribute("i9_formato_orcamento")? Xrm.Page.getAttribute("i9_formato_orcamento").getValue() : null;
	var valor =  Xrm.Page.getAttribute("i9_areadeobra")? Xrm.Page.getAttribute("i9_areadeobra").getValue() : null;
	
	if(startdate == null){
		return;
	}	
	var dia = startdate.getDay();
		
	var numeroExtra = 0;
	if(dia == 0 || dia == 6){
		alert("O inicio não pode ser agendado aos finais de semana");
		Xrm.Page.getAttribute("i9_data_abertura_orcamento").setValue("");
	}

	if(tipo_orcamento == "794480004"){
		if(valor <= 250){
			var numeroSub = 5;
		}else if(valor > 251 && valor <= 500){
			var numeroSub = 6;
		}else if(valor > 501 &&  valor <= 1200){
			var numeroSub = 6;
		}else if(valor > 1201 && valor <= 2000){
			var numeroSub = 9;
		}else if(valor > 2000){
			var numeroSub =12;
		}
	}else if(tipo_orcamento == "794480003"){
		
		if(valor <= 250){
			var numeroSub = 4;
		}else if(valor > 251 && valor <= 500){
			var numeroSub = 5;
		}else if(valor > 501 &&  valor <= 1200){
			var numeroSub = 6;
		}else if(valor > 1201 && valor <= 2000){
			var numeroSub = 8;
		}else if(valor > 2000){
			var numeroSub =10;
		}
	}else if(tipo_orcamento == "794480002"){
		if(valor <= 250){
			var numeroSub = 2;
		}else if(valor > 251 && valor <= 500){
			var numeroSub = 2;
		}else if(valor > 501 &&  valor <= 1200){
			var numeroSub = 3;
		}else if(valor > 1201 && valor <= 2000){
			var numeroSub = 4;
		}else if(valor > 2000){
			var numeroSub = 5;
		}
	}else if(tipo_orcamento == "794480000"){
		if(valor <= 250){
			var numeroSub = 1;
		}else if(valor > 251 && valor <= 500){
			var numeroSub = 1;
		}else if(valor > 501 &&  valor <= 1200){
			var numeroSub = 1;
		}else if(valor > 1201 && valor <= 2000){
			var numeroSub = 1;
		}else if(valor > 2000){
			var numeroSub = 1;
		}
	}else if(tipo_orcamento == "794480001"){
		if(valor <= 250){
			var numeroSub = 1;
		}else if(valor > 251 && valor <= 500){
			var numeroSub = 1;
		}else if(valor > 501 &&  valor <= 1200){
			var numeroSub = 1;
		}else if(valor > 1201 && valor <= 2000){
			var numeroSub = 1;
		}else if(valor > 2000){
			var numeroSub = 1;
		}
	}else if(tipo_orcamento == "794480005"){
		if(valor <= 250){
			var numeroSub = 1;
		}else if(valor > 251 && valor <= 500){
			var numeroSub = 1;
		}else if(valor > 501 &&  valor <= 1200){
			var numeroSub = 1;
		}else if(valor > 1201 && valor <= 2000){
			var numeroSub = 1;
		}else if(valor > 2000){
			var numeroSub = 1;
		}
	}

	startdate.setDate(startdate.getDate()+numeroSub);
	var dia = startdate.getDay();
	var numeroExtra = 0;
	if(dia == 0){
		numeroExtra = 1;
		startdate.setDate(startdate.getDate()+1);
	}else if(dia == 6){
		startdate.setDate(startdate.getDate()+2);
	}

	Xrm.Page.getAttribute("i9_entrega_planejada").setValue(startdate);

}
