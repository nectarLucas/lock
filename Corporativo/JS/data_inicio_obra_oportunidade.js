function calculaDataInicioObra(executionContext){
	
	var formContext = executionContext.getFormContext();
	if(formContext.getAttribute("estimatedclosedate") && formContext.getAttribute("i9_opt_obra_prazoprevistoinicial")){
		estFechamento = formContext.getAttribute("estimatedclosedate").getValue();
		formContext.getAttribute("i9_opt_obra_prazoprevistoinicial").setValue(estFechamento.setDate(estFechamento.getDate() + 15));
	}
}